﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Press : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            Invoke("LoadGame", 0.5f);
        }
	}

    private void LoadGame() {

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
}
