﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour {
    public Player p;
    private Text t;
    private string format;

	// Use this for initialization
	void Start () {
        t = GetComponent<Text>();
        format = t.text;
	}
	
	// Update is called once per frame
	void Update () {
        t.text = string.Format(format, p.score);
	}
}
