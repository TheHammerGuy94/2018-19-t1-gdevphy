﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsSpawner : MonoBehaviour {
    public Obstacle o;
    public float spawnSpeed = 1.0f;
    public float time = 0.0f;
    public float offset = 13f;
    public float ke = 20f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        if (time > 1f / spawnSpeed) {
            SpawnObstacle();
            time -= 1f/spawnSpeed;
        }
	}

    public void SpawnObstacle() {
        Obstacle ox = Instantiate(o);
        float x = Random.Range(0, 2) * 180f;
        float angle = NormalDist(x, 40f) * Mathf.Deg2Rad;
        ox.o = this;
        ox.transform.position = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * offset;
        ox.SpawnBig(ke, Mathf.Pow(Random.Range(1.5f, 2.5f), 2)); 
    }
    public void SpawnChildren(Vector2 loc, Vector2 dir, float mass) {
        Obstacle o1, o2;
        float angle = Mathf.Atan2(dir.y, dir.x)*Mathf.Rad2Deg;
        o1 = Instantiate(o);
        o2 = Instantiate(o);

        o1.o = o2.o = this;
        o1.transform.position = o2.transform.position = loc;

        o1.SpawnSmall(angle +22.5f,ke,mass/2);
        o2.SpawnSmall(angle - 22.5f, ke, mass/2);
    }

    public float NormalDist(float mean, float dev){
        float x = 1.0f - Random.value;
        float y = 1.0f - Random.value;
        float stdNormal = Mathf.Sqrt(-2.0f * Mathf.Log(x)) * Mathf.Sin(2.0f * Mathf.PI * y);
        return mean + dev * stdNormal;
    }


}
