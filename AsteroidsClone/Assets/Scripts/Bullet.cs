﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    public Rigidbody2D rb;
	// Use this for initialization
	void Start () {
        if(!rb)
            rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {

	}
    public void SpawnSettings(float dir, float speed) {
        rb.velocity = new Vector2(Mathf.Cos(dir * Mathf.Deg2Rad), Mathf.Sin(dir * Mathf.Deg2Rad)) * speed;
    }

    public void RotateVel() {
        transform.eulerAngles = new Vector3(0, 0, Mathf.Atan2(rb.velocity.y, rb.velocity.x)*Mathf.Rad2Deg); 
    }

    private void OnCollisionEnter2D(Collision2D c)
    {
        Obstacle o = c.collider.GetComponent<Obstacle>();
        if (o) {
            Game.instance.p.score++;
            o.SpawnMore();
            Destroy(o.gameObject);
            Destroy(gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D c)
    {
        if (c.GetComponent<Game>()) {
            Destroy(gameObject);
        }
    }
}
