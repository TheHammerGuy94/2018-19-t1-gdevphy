﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTo : MonoBehaviour {
    public Vector2 point = Vector2.right;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        RotateTowards();
	}

    public void RotateTowards() {
        Vector2 diff = (point - (Vector2)transform.position);
        transform.eulerAngles = new Vector3(0, 0, Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg-90);
    }
}
