﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour {
    public Bullet b;
    public float distOff = 0.7f;
        public float speed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.anyKeyDown) {
            if (Input.GetMouseButtonDown(0)) {
                SpawnBullet();
            }
        }	
	}

    public void SpawnBullet() {
        Bullet bx = Instantiate<Bullet>(b);
        float angle = transform.eulerAngles.z+90;
        bx.transform.position = transform.position + new Vector3(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle*Mathf.Deg2Rad)) * distOff;
        bx.SpawnSettings(angle, speed);
        bx.RotateVel();
    }




}
