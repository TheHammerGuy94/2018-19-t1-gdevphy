﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {
    public Rigidbody2D rb;
    public ObsSpawner o;
    public bool spawnMore = true;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void SpawnBig(float ke, float mass) {
        rb.mass = mass;
        transform.localScale = new Vector3(Mathf.Sqrt(mass), Mathf.Sqrt(mass), 1.0f);
        rb.velocity = -transform.position.normalized * GetSpeed(ke, mass);
    }
    public void SpawnSmall(float angle,float ke, float mass) {
        float size = Mathf.Sqrt(mass) / 2f;
        rb.mass = mass;
        transform.localScale = new Vector3(Mathf.Sqrt(mass), Mathf.Sqrt(mass), 1.0f);
        transform.position += new Vector3(Mathf.Cos((angle) * Mathf.Deg2Rad), Mathf.Sin((angle) * Mathf.Deg2Rad)) * (size / 2);
        rb.velocity = new Vector2(Mathf.Cos((angle) * Mathf.Deg2Rad), Mathf.Sin((angle) * Mathf.Deg2Rad)) * GetSpeed(ke, mass / 2);
    }

    public float GetSpeed(float ke, float mass) {
        return Mathf.Sqrt(ke * 2 / mass);
    }

    public float KineticEnergy() {
        return (rb.mass * rb.velocity.sqrMagnitude) / 2f;
    }
    public void SpawnMore() {
        o.SpawnChildren(transform.position, rb.velocity, rb. mass);
    }

    private void OnTriggerExit2D(Collider2D c)
    {
        if (c.GetComponent<Game>())
        {
            spawnMore = false;
            Destroy(gameObject);
        }
    }
    


}
