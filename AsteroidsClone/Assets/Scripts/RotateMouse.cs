﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateMouse : MonoBehaviour {
    private RotateTo rote;

    // Use this for initialization
    void Start () {
        rote = GetComponent<RotateTo>();
	}

    // Update is called once per frame
    void Update () {
        rote.point =  Camera.main.ScreenToWorldPoint(Input.mousePosition);
	}
}
