﻿using UnityEngine;
using System.Collections;

public class CannonRotation : Cannon
{
    public float roteSpeed = 10.0f;
    private float sign = 0;
    public override void GetInput()
    {
        if (ChangeCondition())
            sign = 0;
        if(Input.anyKey){
            if (Input.GetKey(KeyCode.UpArrow))
                sign += 1;
            if (Input.GetKey(KeyCode.DownArrow))
                sign -= 1;
        }
    }
    
    public override bool ChangeCondition()
    {
        return Mathf.Abs(sign) > 0;
    }
    
    public override void ApplyChange()
    {
        sign = Mathf.Sign(sign);
        RotateCannon(roteSpeed * sign * Time.deltaTime);
    }


    public void RotateCannon(float angle){
        if(t.eulerAngles.z+angle <90 && t.eulerAngles.z + angle > 0)
            t.Rotate(0, 0, angle);
    }
}
