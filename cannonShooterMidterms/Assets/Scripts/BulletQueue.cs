﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletQueue : MonoBehaviour {
    public static BulletQueue instance = null;
    private Queue<Ball> q;
    public int maxSize = 10;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    // Use this for initialization
    void Start () {
        q = new Queue<Ball>();
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void DestroyFirst(){
        Ball b = q.Dequeue();
        Destroy(b.gameObject);
    }

    public void NewBall(Ball b){
        q.Enqueue(b);
        while (q.Count > maxSize)
            DestroyFirst();
    }
    public void DestroyedBall(Ball b) {
        Ball bx = q.Dequeue();
        while (bx != b) {
            q.Enqueue(bx);
            bx = q.Dequeue();
        };
    }


}
