﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerUI : MonoBehaviour {
    private Text txt;
    private string format;
    public Timer t;

	// Use this for initialization
	void Start () {
        txt = GetComponent<Text>();
        format = txt.text;
	}
	
	// Update is called once per frame
	void Update () {
        txt.text = string.Format(format, t.timer.ToString("0.00"));
	}
}
