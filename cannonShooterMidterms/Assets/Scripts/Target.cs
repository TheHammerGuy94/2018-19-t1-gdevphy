﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

    private Collider2D cd;
    private MovingObject mv;
    public float minX, maxX;
    public float minY, maxY;

    public CannonMovement cnon;
    public TimerUI UIfin;

	// Use this for initialization
	void Start () {
        cd = GetComponent<Collider2D>();
        if (cnon == null)
            cnon = GameObject.Find("cannon").GetComponent<CannonMovement>();
        if (mv == null)
            mv = GetComponent<MovingObject>();
        UIfin.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D c)
    {

        if(c.gameObject.GetComponent<Ball>()){
            //mv.gravity = 1;
        }
    }

    private void OnCollisionEnter2D(Collider2D c)
    {
    }

    private void setVisible(bool x){
        this.GetComponent<SpriteRenderer>().enabled = x;
    }
    public void appear(){
        setVisible(true);
        this.transform.position = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 0);
    }
    public void Win() {
        Game.instance.Win();
        UIfin.gameObject.SetActive(true);
    }
}
