﻿using UnityEngine;
using System.Collections;

public class Game: MonoBehaviour
{
    public BulletQueue q;
    public static Game instance = null;
    public int score = 0,shots = 0;
    public bool speedup = false;
    private bool speedPrev = false;
    public Timer t;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
    // Use this for initialization
    void Start()
    {
        if (t == null)
            t = GetComponent<Timer>();


    }

    // Update is called once per frame
    void Update()
    {
        if(Input.anyKeyDown){
            if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
                speedup = !speedup;
        }

    }

    private void SpeedUp(){
        if(speedup != speedPrev){
            speedPrev = speedup;
            Time.timeScale = speedPrev ? 4 : 1;
        }
    }
    public void AddScore(){
        score++;
    }

    public void AddShot(){
        shots++;
    }

    public float GetAccuracy(){
        return (shots == 0)? 0f:(float)score / (float)shots;
    }
    public void Win() {
        t.play = false;
    }

    private void OnTriggerExit2D(Collider2D c)
    {
        Target t = c.gameObject.GetComponent<Target>();
        if (t != null) {
            t.Win();
        }
        
    }
    
}
