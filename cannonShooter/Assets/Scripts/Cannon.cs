﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Cannon : MonoBehaviour {
    protected Transform t;

	// Use this for initialization
	void Start () {
        t = GetComponent<Transform>();

	}
	
	// Update is called once per frame
	void Update () {
        //step 1: get keyboardInput;
        GetInput();
        //step 2: isInput direction >= 1?
        if(ChangeCondition())
            ApplyChange();  //step 3: if yes, move(inputDir*speed);

	}
	//takes the input from he keyboard 
    public abstract void GetInput();
    //what is the change condition
    public abstract bool ChangeCondition();
    //apply the change
    public abstract void ApplyChange();


}
