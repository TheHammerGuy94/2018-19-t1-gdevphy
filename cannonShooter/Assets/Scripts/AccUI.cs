﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AccUI : MonoBehaviour {
    private Text txt;
    private string format;

    private float prevAcc = -1f;



	// Use this for initialization
	void Start () {
        txt = GetComponent<Text>();
        format = txt.text;

	}
	
	// Update is called once per frame
	void Update () {
        if(prevAcc.CompareTo(Game.instance.GetAccuracy())!= 0){
            prevAcc = Game.instance.GetAccuracy();
            UpdateText();
        }
	}

    public void UpdateText(){
        txt.text = string.Format(format, Game.instance.GetAccuracy().ToString("p2"));

    }
}
