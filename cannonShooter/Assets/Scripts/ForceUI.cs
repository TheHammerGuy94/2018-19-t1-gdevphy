﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ForceUI : MonoBehaviour {
    private Text txt;
    private string format;

    public CannonShooter shoot;
    private float prevForce = -1f;

    // Use this for initialization
    void Start () {
        txt = GetComponent<Text>();
        format = txt.text;
        if (shoot == null)
            shoot = FindObjectOfType<CannonShooter>();
    }
	
	// Update is called once per frame
	void Update () {
        if(prevForce.CompareTo(shoot.shootingForce)!=0){
            prevForce = shoot.shootingForce;
            UpdateText();
        }
    }
    public void UpdateText(){
        txt.text = string.Format(format, shoot.shootingForce.ToString("f4"));
    }
}
