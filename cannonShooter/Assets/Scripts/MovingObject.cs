﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : MonoBehaviour {
    public Collider2D cd;
    public Vector2 summationForces = new Vector2(0,0);
    public Vector2 velocity = new Vector2(0,0);
    public float gravity = 0.0f;
    public float mass = 1.0f;

    private Vector2 facingDirection = new Vector2(0);

	// Use this for initialization
	void Start () {
        cd = GetComponent<Collider2D>();
	}

    private void FixedUpdate()
    {
        ApplyGravity();
        move();
        if (velocity.getScale() < 0.01f)
            velocity *= 0;
        //applyDrag(cd.friction);
        Rotate();
    }

    // Update is called once per frame
    void Update () {
        //ClampBounds();
	}

    public void ApplyGravity(){
        this.summationForces += new Vector2(0, -9.8f * gravity * mass);
    }

    public void applyForce(Vector2 force){
        summationForces += force / mass;
    }

    public void applyDrag(float amount){
        Vector2 dragForce = this.velocity * amount *amount;
        applyForce(-1*dragForce);
    }

    private void move()
    {
        MoveObject(velocity * Time.deltaTime);
        if (velocity.getScale() > 0.0f) 
            facingDirection = velocity.Duplicate().Normalize();
        velocity += summationForces * Time.deltaTime;
        summationForces *= 0;
    }

    private void MoveObject(Vector2 v) {
        gameObject.transform.position = new Vector3(gameObject.transform.position.x + v.x, gameObject.transform.position.y+v.y, 0f);
    }

    private void OnCollisionEnter2D(Collision2D c)
    {
        Debug.Log("HIT SOMETHING!!!");
        float bounce = c.collider.bounciness * cd.bounciness;
        float friction = c.collider.friction * cd.friction;
        Vector2 normal = Vector2.GetCustomVector2(c.contacts[0].normal).rotateBy(+90);
        
        Debug.DrawLine(c.contacts[0].point + c.contacts[0].normal, c.contacts[0].point, Color.green);
        //Debug.Break();


        MovingObject m = c.gameObject.GetComponent<MovingObject>();
        if (m != null)
        {
            m.applyForce(normal.rotateBy(-90) * mass * this.velocity.getScale() * Time.fixedDeltaTime);
        }
        else
            velocity = Vector2.Reflect(velocity, normal) * bounce;
        //applyDrag(friction);
    }

    private void Rotate(){
        transform.rotation = Quaternion.Euler(0, 0, velocity.getAngle());
    }

    private void ClampBounds(){
        float x = transform.position.x, y = transform.position.y;
       
        if (x < -9f && velocity.x <0){
            x = -8.99f;
            velocity = Reflect(velocity, Vector3.left);
        }
        if (x > 9f && velocity.x > 0) {
            x = 8.99f;
            velocity = Reflect(velocity, Vector3.left);
        }

        if (y < -5.4f){
            y = -5.39f;
            velocity = Reflect(velocity, Vector3.up);
        }
        if (y > 5.4f){
            y = 5.39f;
            velocity = Reflect(velocity, Vector3.up);
        }

        transform.position = new Vector3(x, y, transform.position.z);

    }

    public static float DClamp(float x, float min, float max) {
        if (x < min) return min;
        else if (x > max) return max;
        else return x;
    }
    public static Vector3 convertVector2(Vector2 v){
        return new Vector3(v.x, v.y, 0);
    }
    public static Vector2 convertVector3(Vector3 v){
        return new Vector2(v.x, v.y);
    }
    public static Vector2 Reflect(Vector2 target, Vector3 normal){
        return convertVector3(Vector3.Reflect(convertVector2(target), normal));
    }
}
