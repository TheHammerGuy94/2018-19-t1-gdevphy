﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{
    private Collider2D cd;
    private MovingObject mv;
    // Use this for initialization
    void Start()
    {
        cd = GetComponent<Collider2D>();
        mv = GetComponent<MovingObject>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnCollisionEnter2D(Collision2D c)
    {
        
    }
    private void OnDestroy()
    {
        Game.instance.q.DestroyedBall(this);
    }

}
