﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreUI : MonoBehaviour
{
    private Text txt;
    private string format;
    private int prevScore = -1;

    // Use this for initialization
    void Start()
    {
        txt = GetComponent<Text>();
        format = txt.text;
    }

    // Update is called once per frame
    void Update()
    {
        if(prevScore != Game.instance.score){
            prevScore = Game.instance.score;
            UpdateText();

        }
    }

    public void UpdateText(){
        txt.text = string.Format(format, Game.instance.score);
    }
}
