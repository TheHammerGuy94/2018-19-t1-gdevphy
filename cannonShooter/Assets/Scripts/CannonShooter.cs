﻿using UnityEngine;
using System.Collections;

public class CannonShooter : MonoBehaviour
{
    private Transform t;
    public GameObject ball;

    public float fireDelay = 1.0f;
    public float time = 0.0f;

    public float shootingForce = 100;
    public float maxForce = 1000f;
    public float minForce = 400f;
    public float speedChange = 10f;
    private float percent = 0.0f;

    // Use this for initialization
    void Start()
    {
        t = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;


        if (Input.GetKey(KeyCode.Space)) {
            percent += Time.deltaTime;
            shootingForce = minForce + speedChange * percent;
            shootingForce = Mathf.Min(shootingForce, maxForce);
        }
        if (Input.GetKeyUp(KeyCode.Space)) {
            percent = 0.0f;
            SpawnBall();
        }


    }
    public void SpawnBall(){
        GameObject o = Instantiate(ball);
        o.transform.position = t.position;
        MovingObject mo = o.GetComponent<MovingObject>();
        mo.applyForce(new Vector2(t.eulerAngles.z) * Random.Range(shootingForce*0.95f, shootingForce * 1.05f));
        Game.instance.AddShot();

        BulletQueue.instance.NewBall(o.GetComponent<Ball>());

    }

}
