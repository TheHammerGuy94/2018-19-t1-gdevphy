﻿using UnityEngine;
using System.Collections;

public class CannonMovement : MonoBehaviour
{
    public float minX, maxX, minY, maxY;
    public Vector2 direction = new Vector2();

    private void Start()
    {

    }

    private void Update()
    {

    }

    public void Teleport(){
        this.transform.position = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY),0f);
    }
}
