﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum Direction
{
    north,south, west, east
}

public static class DirectionMethods {
    public static Vector2 GetVector(this Direction x) {
        switch (x) {
            default: return new Vector2();
            case Direction.north: return new Vector2(90);
            case Direction.south: return new Vector2(270);
            case Direction.west: return new Vector2(180);
            case Direction.east: return new Vector2(0);
        }
    }
}
