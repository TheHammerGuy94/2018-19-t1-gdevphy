﻿using System;
using System.Collections;
using System.Collections.Generic;

public class Vector2{
    public float x, y;

    public Vector2() {
        x = y = 0.00f;
    }

    public Vector2(float x, float y) {
        setXY(x, y);
    }

    public Vector2(float deg) {
        SetAngleNorm(deg);
    }

    public Vector2 setXY(float x, float y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public Vector2 setDirection(float deg) {
        float mag = this.getScale();
        this.SetAngleNorm(deg);
        this.x *= mag;
        this.y *= mag;
        return this;
    }


    public Vector2 setDirection(Vector2 targ){
        targ = targ.Normalize().Duplicate();
        float mag = this.getScale();
        this.x = targ.x; this.y = targ.y;
        this.x *= mag; this.y *= mag;
        return this;
    }


    //this rotates Vector relative to its current direction
    //adds to it's current direction
    public Vector2 rotateBy(float deg) {
        return this.SetAngleNorm(this.getAngle() + deg);
    }

    
    public Vector2 SetAngleNorm(float deg){
        this.x = (float)Math.Cos(Deg2Rad(deg));
        this.y = (float)Math.Sin(Deg2Rad(deg));
        return this;
    }

    public Vector2 Duplicate() {
        return new Vector2(this.x, this.y);
    }

    public float getScale() {
        return (float) Math.Sqrt(this.x * this.x + this.y * this.y);
    }

    public float getAngle(){
        float x = Rad2Deg((float)Math.Atan2(this.y, this.x));
        if (x < 0)
            x += 360;
        return x;
    }

    public Vector2 Normalize() {
        float mag = getScale();
        this.x /= mag;
        this.y /= mag;
        return this;
    }

    public static float Deg2Rad(float deg){
        return deg *(float) Math.PI / 180f;
    }

    public static float Rad2Deg(float rad) {
        return rad * 180f / (float) Math.PI;
    }

    public static Vector2 operator +(Vector2 a, Vector2 b){
        return new Vector2(a.x + b.x, a.y + b.y);
    }

    public static Vector2 operator -(Vector2 a, Vector2 b){
        return new Vector2(a.x - b.x, a.y - b.y);
    }

    public static Vector2 operator *(Vector2 a, float b){
        return new Vector2(a.x * b, a.y * b);
    }

    public static Vector2 operator *(float b, Vector2 a){
        return a * b;
    }

    public static Vector2 operator /(Vector2 a, float b){
        return new Vector2(a.x / b, a.y / b);
    }

}
