﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : MonoBehaviour {
    private static float gravity = 9.8f;
    public Vector2 summationForces = new Vector2(0,0);
    public Vector2 velocity = new Vector2(0,0);
    public float gravityScale = 1.0f;
    public float mass = 1.0f;

    private Vector2 facingDirection = new Vector2(0);

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {


        ApplyGravity();
        move();
        Rotate();

        ClampBounds();
        CollideFloor();
	}

    public void applyForce(Vector2 force){
        summationForces += force;
    }

    private void move()
    {
        MoveObject(velocity * Time.deltaTime);
        if (velocity.getScale() > 0.0f) 
            facingDirection = velocity.Duplicate().Normalize();
        velocity += summationForces * Time.deltaTime;
        summationForces *= 0;
    }

    private void MoveObject(Vector2 v) {
        gameObject.transform.position = new Vector3(gameObject.transform.position.x + v.x, gameObject.transform.position.y+v.y, 0f);
    }
    private void Rotate(){
        transform.rotation = Quaternion.Euler(0, 0, velocity.getAngle());
    }
    private void CollideFloor() {
        float y = transform.position.y;
        if (y <= -5 && velocity.y <0) {
            y = -5;
            this.velocity.y *= 0;
        }
        transform.position = new Vector3(transform.position.x, y);

    }

    private void ClampBounds(){
        float x = transform.position.x, y = transform.position.y;
        if (x < -9f) x = 8.99f;
        if (x > 9f) x = -8.99f;

        if (y < -5.4f) y = 5.39f;
        //if (y > 5.4f) y = -5.39f;

        transform.position = new Vector3(x, y, transform.position.z);

    }

    private void ApplyGravity() {
        applyForce(Direction.south.GetVector() * gravity * gravityScale);
    }
    

    public static float DClamp(float x, float min, float max) {
        if (x < min) return min;
        else if (x > max) return max;
        else return x;
    }
}
