﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyBoardAccel : MonoBehaviour {
    private MovingObject m;
    public float forceScale = 5;

	// Use this for initialization
	void Start () {
        m = gameObject.GetComponent<MovingObject>();

	}
	
	// Update is called once per frame
	void Update () {
        if(Input.anyKey){
            if (Input.GetKey(KeyCode.W)) m.applyForce(Direction.north.GetVector() * forceScale);
            if (Input.GetKey(KeyCode.S)) m.applyForce(Direction.south.GetVector() * forceScale);
            if (Input.GetKey(KeyCode.D)) m.applyForce(Direction.east.GetVector() * forceScale);
            if (Input.GetKey(KeyCode.A)) m.applyForce(Direction.west.GetVector() * forceScale);
        }
	}
}
