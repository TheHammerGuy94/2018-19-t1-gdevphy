﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour {
    private MovingObject o;
    public float jumpForce = 1.0f;

	// Use this for initialization
	void Start () {
        o = GetComponent<MovingObject>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.anyKeyDown) {
            if (Input.GetKeyDown(KeyCode.Space)) {
                o.applyForce(Direction.north.GetVector() * jumpForce);
            }
        }	
		
	}
}
